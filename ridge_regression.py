"""
This code file contains the code for the Ridge Regression ADAM
"""

import numpy as np
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle

class Ridge():
    def __init__(self, learning_rate, epochs, batch_size, lambda_value=1, beta1=0.9, beta2=.999, eps=1e-8, verbose=True):
        """
        learning_rate: (float) learning rate for ADAM
        epochs: (int) number of epochs to perform ADAM for
        lambda_value: L2 value used for regularization
        batch_size: (int) size of each batch of training data
        beta1:
        beta2:
        eps: epsilon value used for ADAM

        """
        self.learning_rate = learning_rate #alppha
        self.epochs = epochs
        self.lambda_value = lambda_value
        self.batch_size = batch_size
        self.verbose = verbose

        self.beta1 = beta1
        self.beta2 = beta2
        self.eps = eps

        self.weights = []

        self.X = []
        self.y = []

    # derivative of objective function
    def derivative(self, X, y):
        return np.asarray([X * 2.0, y * 2.0])

    def __loss(self):
        prediction = np.dot(self.X, self.weights)
        difference = self.y - prediction
        return np.dot(difference.T, difference)

    def __gradient(self):

        M, N = self.X.shape

        prediction = self.predict(self.X)
        error = self.y - prediction

        m = [0, 0]
        v = [0, 0]

        g = self.derivative(self.X[0], self.X[1])

        # calculate the moments (mean and variance)
        m = [self.beta1 * m_i + (1 - self.beta1) * g_i for m_i, g_i in zip(m, g)]
        v = [self.beta2 * v_i + (1 - self.beta2) * (g_i**2) for v_i, g_i in zip(v, g)]

        # bias correction
        m_hat = [m_i / (1 - (self.beta1**self.t)) for m_i, g_i in zip(m, g)]
        v_hat = [v_i / (1 - (self.beta2**self.t)) for v_i, g_i in (v, g)]

        # Performs L2 regularization, i.e.adds penalty equivalent to square of the magnitude of coefficients
        regularization_term = (- (2 * np.dot(self.X.T, error)) + (2 * self.lambda_value * self.weights)) / M

        # update weights with regularization term
        self.weights = self.weights - (self.learning_rate / (np.sqrt(v_hat[0]) + self.eps))*m_hat[0] + regularization_term
        return self

    def fit(self, X, y):
        """
        Inputs

        X: (numpy float array) Feature vectors to predict values for
        y: (numpy float array) ground truth target values to train on
        """
        self.X = X
        self.y = y

        # initialize weights
        self.weights = np.random.normal(size=np.shape(X)[-1])

        # gradient descent w/ ADAM optimizer
        for e in range(self.epochs):
            self.t = 1

            X, y = shuffle(X, y)
            self.__gradient()

            self.t = self.t + 1
            if self.verbose:
                print("Loss :" + str(self.__loss()))

    def predict(self, X):
        """
            Inputs

        X: (numpy float array) Feature vectors to predict values for
        """
        # returns the dot product between X and weights
        # no need for batch bc not distributed across anything
        return np.dot(X, self.weights)

def main ():
    # Import Dataset
    data = datasets.load_diabetes()
    X = data.data
    Y = data.target

    # Split dataset
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.3, random_state=0)

    # train model
    model = Ridge(epochs=1000, learning_rate=0.01, batch_size=1)
    model.fit(X_train, Y_train)

    # prediction on test set
    Y_pred = model.predict(X_test)
    print("Y_test - Y-pred          ", Y_test - Y_pred)
    print("Predicted values         ", np.round(Y_pred[:3], 2))
    print("Real values              ", Y_test[:3])
    print("Trained weights          ", round(model.weights[0], 2))
    print("Trained batch            ", round(model.batch_size, 2))
    print("Mean Value Square Error: ", (np.sum(Y_test - Y_pred)**2))

if __name__ == "__main__":
    main()
